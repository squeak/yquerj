var _ = require("underscore");
var $ = require("jquery");
var dompurify = require("dompurify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DETAILED ERROR
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: log error clearly
  ARGUMENTS: ({
    !type: <string>,
    !color: <string>,
    !message: <string>,
    ?args: <any[]>,
  })
  RETURN: void
*/
function logError (options) {
  console.log("%c————————————————————————————————————————————————————————", "color:"+ options.color);
  console.log("%c!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", "color:"+ options.color);
  // log error context and message with color
  console.log("%c" + "yquerj "+ options.type, "color:"+ options.color);
  console.log("%c" + options.message, "font-weight:bold; color:"+ options.color);
  // log other arguments
  if (options.args) _.each(options.args, function (toLog) { console.error(toLog); });
  console.log("%c¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡", "color:"+ options.color);
  console.log("%c————————————————————————————————————————————————————————", "color:"+ options.color);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LIST OF SUPPORTED TAGS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var supportedTags = {
  div: "<div/>",
  a: "<a/>",
  img: "<img>",
  span: "<span/>",
  p: "<p/>",
  pre: "<pre/>",
  h1: "<h1/>",
  svg: "<svg/>",
  g: "<g/>",
  path: "<path/>",
  canva: "<canvas>",
  // canvas: "<canvas>", // not this or no canvas works anymore
  object: "<object/>",
  embed: "<embed>",
  link: "<link>",
  formTag: "<form/>",
  video: "<video/>",
  audio: "<audio/>",
  source: "<source/>",
  iframe: "<iframe/>",
  fieldset: "<fieldset/>",
  legend: "<legend/>",
  // inputs
  input: "<input/>",
  label: "<label/>",
  buttonTag: "<button>",
  textarea: "<textarea/>",
  // selects inputs
  selectTag: "<select/>",
  optionTag: "<option/>",
  // tables
  table: "<table/>",
  thead: "<thead/>",
  tbody: "<tbody/>",
  td: "<td/>",
  th: "<th/>",
  tr: "<tr/>",
  // lists
  ul: "<ul/>",
  li: "<li/>",
  //
  br: "<br/>",
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  TAG CREATION FUNCTION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// support append (implicit) and prepend (to specify explicitly)
function createTag (tag, attributesOrClassList, appendOrPrepend) {

  //
  //                              ATTRIBUTES OR SIMPLE LIST OF CLASSES

  var attributes = _.isString(attributesOrClassList) ? { class: attributesOrClassList } : attributesOrClassList;

  //
  //                              MAKE SURE appendOrPrepend IS EITHER "append" OR "prepend"

  if (appendOrPrepend != "append" && appendOrPrepend != "prepend") {

    // if it's not undefined but something else, alert error
    if (!_.isUndefined(appendOrPrepend)) logError({
      type: "ALERT",
      color: "orange",
      message: "'appendOrPrepend' value not recognized, default ('append') has been used.\nThis is the value you used that wasn't recognized:",
      args: [ appendOrPrepend, ],
    });

    appendOrPrepend = "append";

  };

  //
  //                              CREATE ASKED ELEMENT

  if (this[0] instanceof Element) {
    // make element
    var $elem = $(tag, attributes)[appendOrPrepend +"To"](this)
    // return generated element
    return $elem;
  }
  else {
    logError({
      type: "ERROR",
      color: "red",
      message: "the element you tried to append child in doesn't seem to be a proper jquery object containing elements",
      args: [ this, ],
    });
    return this;
  };

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE TAGS METHODS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

if (!$.fn) console.error("\033[1;31myquerj ERROR:\033[0;31m $.fn doesn't seem to exist! If you are importing from nodejs side, yquerj will not be anything more than a basic jquery.\033[0m ")
else _.each(supportedTags, function (tagHtml, tagName) {
  $.fn[tagName] = _.partial(createTag, tagHtml);
});

// for (key in supportedTags) $.fn[key] = new Function ("attr", "append", "append = append || 'append'; return $(supportedTags['"+ key +"'], attr)[append +'To'](this)");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ADDITIONAL METHODS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

if ($.fn) {

  /**
    DESCRIPTION: will add the given string to this element, just like $().html() does, but will make sure string was sanitized before
    ARGUMENTS: ( !stringOrFunction <
      | <string> « html string for this element »
      | <function( $element <yquerjObject> )> « function to create content in the element »
    > )
    RETURN: <this.html·result>
  */
  $.fn.htmlSanitized = function (stringOrFunction) {
    if (_.isString(stringOrFunction) || _.isNumber(stringOrFunction)) return this.html(dompurify.sanitize(stringOrFunction +"", { SAFE_FOR_JQUERY: true }))
    else if (_.isFunction(stringOrFunction)) {
      var result = stringOrFunction(this);
      if (!_.isUndefined(result)) this.htmlSanitized(result);
    }
    else if (stringOrFunction) logError({
      type: "ERROR",
      color: "red",
      message: "yquerj htmlSanitized only supports strings, numbers and functions",
      args: [ "You passed: ", stringOrFunction, ],
    });
  };

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = $;
