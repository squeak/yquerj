# List of all supported tags

Due to the naming of some jquery methods, some tags have a different naming in yquerj.
For example: `$("body").formTag()` will create a `<form></form>` element.

Full list of supported tags and how to call them
```javascript
var supportedTags = {
  div: "<div/>",
  a: "<a/>",
  img: "<img>",
  span: "<span/>",
  p: "<p/>",
  pre: "<pre/>",
  h1: "<h1/>",
  svg: "<svg/>",
  g: "<g/>",
  path: "<path/>",
  canva: "<canvas>",
  object: "<object/>",
  embed: "<embed>",
  link: "<link>",
  formTag: "<form/>",
  video: "<video/>",
  audio: "<audio/>",
  source: "<source/>",
  iframe: "<iframe/>",
  fieldset: "<fieldset/>",
  legend: "<legend/>",
  input: "<input/>",
  label: "<label/>",
  buttonTag: "<button>",
  textarea: "<textarea/>",
  selectTag: "<select/>",
  optionTag: "<option/>",
  table: "<table/>",
  thead: "<thead/>",
  tbody: "<tbody/>",
  td: "<td/>",
  th: "<th/>",
  tr: "<tr/>",
  ul: "<ul/>",
  li: "<li/>",
  br: "<br/>",
};
```
