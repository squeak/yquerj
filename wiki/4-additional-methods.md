# Additional methods

In addition to tag creating convenience methods, yquerj provides the following method(s):

## htmlSanitized

Same as jquery's `html` method (`$().html()`) `htmlSanitized` method sets the html content of the target(s) but sanitizing the passed content with [dompurify](https://github.com/cure53/DOMPurify).

For example, the result of: `$("body").htmlSanitized(`"&lt;div&gt;hey&lt;/div&gt;&lt;script&gt;alert('HEYYYYYY')&lt;/script&gt;"`)` will be the following dom:
```html
<body>
  <div>
    hey
  </div>
</body>
```

You can also used `htmlSanitized`, passed as argument when creating an element.
For example:
`$("body").div({`
  `htmlSanitized:` '&lt;script&gt;alert("This script will be removed by dompurify, so this alert won\'t be shown.")&lt;/script&gt;hello'
`});`

If you pass a function to `htmlSanitized`, instead of using $.html to fill the element, it will run the passed function and pass it the element so the function can complexly fill it. If the function returns a value, will use $.htmlSanitized again to add it to the element.
