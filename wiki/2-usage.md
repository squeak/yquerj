# Usage

Load and use yquerj library with:

```javascript
const $ = require("yquerj");
let $myDiv = $("body").div({
  class: "my-div",
  text: "hello",
});
$myDiv.span();
```
The resulting html will look like this:
```html
<body>
  <div class="my-div">
    hello
    <span></span>
  </div>
</body>
```
