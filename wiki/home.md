
A version of jquery extended with convenient utilities to create dom elements.
In addition to what jquery can do, yquerj allows you to conveniently create dom elements.
You can easily create a `<div>` in any container with:
```javascript
$(".myContainer").div({  class: "my-class", text: "hi", })
```
or:
```javascript
$(".myContainer").div("my-class")
```
or a `<span>`:
```javascript
$(".myContainer").span({ class: "my-class", text: "hi", })
```
...
